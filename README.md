# simple-web-clock :clock9:
<img src="https://user-images.githubusercontent.com/70860732/112530303-3183c300-8d9e-11eb-98d3-5e7b6d7b5a00.jpeg" align="right" width="50%">

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/simple-web-clock/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/simple-web-clock?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/simple-web-clock?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/simple-web-clock?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/simple-web-clock)](https://replit.com/@KennyOliver/simple-web-clock)

**A very simple clock website — my first JS project!**

[![Website Link](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/simple-web-clock/)](https://kennyoliver.github.io/simple-web-clock/)

## neumorphia.css :art: :package:
**simple-web-clock** uses **[neumorphia.css](https://github.com/KennyOliver/neumorphia.css)** – my own CSS framework!

[![neumorphia.css](https://img.shields.io/badge/Get%20neumorphia.css-75D2AF?style=for-the-badge&logo=css3&logoColor=252525&link=https://kennyoliver.github.io/neumorphia.css/neumorphia.css)](https://kennyoliver.github.io/neumorphia.css/neumorphia.css)

---
Kenny Oliver ©2021
